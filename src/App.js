import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component{
  constructor (props){
    super(props);
  
    this.state = {identi:0, nombre:'', cantidad:0, precio:0,identi2:0, nombre2:'', cantidad2:0, precio2:0, r:[{arreglo:[]}]};
    }

    insertar = () => {
      fetch("http://localhost/P2/Insertar.php?nombre="+this.state.nombre+"&cantidad="+this.state.cantidad+"&precio="+this.state.precio)
      .then((response) => response.json())
      .then((responseJson) => {
        alert('Se ingreso el numero');
        })
      .catch((error) => {
        console.log(error);
        
      });
  
    }
  

    actualizar = () => {
      fetch("http://localhost/P2/Actualizar.php?identificacion="+this.state.identi2+"&nombre="+this.state.nombre2+"&cantidad="+this.state.cantidad2+"&precio="+this.state.precio2)
      .then((response) => response.json())
      .then((responseJson) => {
        alert('Se ingreso el numero');
        })
      .catch((error) => {
        console.log(error);
        
      });
  
    }


    consultarcant = () => {
      fetch("http://localhost/P2/Consultarcant.php")
    .then((response) => response.json())
    .then((responseJson) => {
      //alert(responseJson.respuesta);
      var a='';
      for (let i = 0; i < responseJson.length; i++) {
        var b = responseJson[i].id +' '+ responseJson[i].nombre+' '+ responseJson[i].cantidad+' '+ responseJson[i].precio;
        a+=b + '\n';  
        console.log(b);
      }
      this.setState({r:a});
      console.log(responseJson);
      
     
    })
    .catch((error) => {
      console.log(error);
    });
    }

    consultarprec = () => {
      fetch("http://localhost/P2/Consultarprec.php")
    .then((response) => response.json())
    .then((responseJson) => {
      //alert(responseJson.respuesta);
      var a='';
      for (let i = 0; i < responseJson.length; i++) {
        var b = responseJson[i].id +' '+ responseJson[i].nombre+' '+ responseJson[i].cantidad+' '+ responseJson[i].precio;
        a+=b + '\n';  
        console.log(b);
      }
      this.setState({r:a});
      console.log(responseJson);
      
     
    })
    .catch((error) => {
      console.log(error);
    });
    }


    consultar = () => {
      fetch("http://localhost/P2/Consultar.php")
    .then((response) => response.json())
    .then((responseJson) => {
      //alert(responseJson.respuesta);
      var a='';
      for (let i = 0; i < responseJson.length; i++) {
        var b = responseJson[i].id +' '+ responseJson[i].nombre+' '+ responseJson[i].cantidad+' '+ responseJson[i].precio;
        a+=b + '\n';  
        console.log(b);
      }
      this.setState({r:a});
      console.log(responseJson);
      
     
    })
    .catch((error) => {
      console.log(error);
    });
    }

    eliminar = () => {
      fetch("http://localhost/P2/Eliminar.php?identificacion="+this.state.identi)
    .then((response) => response.json())
    .then((responseJson) => {
      //alert(responseJson.respuesta);
      alert('Se elimino el producto');
    })
    .catch((error) => {
      console.log(error);
    });

    }

render() {
  return (
    <div className="App">
     <h2>Registro y consulta</h2>
    Ingrese el nombre <input type="text" value={this.state.nombre} onChange={(name)=>{this.setState({nombre:name.target.value})}}></input>  
    <br></br>
    Ingrese la cantidad <input type="number" value={this.state.cantidad} onChange={(cant)=>{this.setState({cantidad:cant.target.value})}}></input>
    <br></br>
    Ingrese el precio <input type="number" value={this.state.precio} onChange={(prec)=>{this.setState({precio:prec.target.value})}}></input>
    <br></br>
    <button value="Insertar" onClick={this.insertar}>Insertar</button>
    <button value="consultarc" onClick={this.consultarcant}>Consultar por cantidad</button>
    <button value="consultarp" onClick={this.consultarprec}>Consultar por precio</button>
    <button value="consultar" onClick={this.consultar}>Consultar todo</button>
    <br></br>
    <textarea value={this.state.r} readOnly={true}></textarea>
    <br></br>
    <br></br>

    <h2>Eliminar registro</h2>
    Ingrese el id<input type="number" value={this.state.identi} onChange={(cod)=>{this.setState({identi:cod.target.value})}}></input>
    <button value="eliminar" onClick={this.eliminar}>Eliminar</button>
    <br></br>

    <h2>Actualizar producto</h2>
    Ingrese el id<input type="number" value={this.state.identi2} onChange={(cod)=>{this.setState({identi2:cod.target.value})}}></input>
    <br></br>
    Ingrese el nombre <input type="text" value={this.state.nombre2} onChange={(name)=>{this.setState({nombre2:name.target.value})}}></input>  
    <br></br>
    Ingrese la cantidad <input type="number" value={this.state.cantidad2} onChange={(cant)=>{this.setState({cantidad2:cant.target.value})}}></input>
    <br></br>
    Ingrese el precio <input type="number" value={this.state.precio2} onChange={(prec)=>{this.setState({precio2:prec.target.value})}}></input>
    <br></br>
    <button value="actualizar" onClick={this.actualizar}>Actualizar producto</button>
    </div>
    
  );
}
}
export default App;
